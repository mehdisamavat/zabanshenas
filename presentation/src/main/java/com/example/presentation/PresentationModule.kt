package com.example.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.presentation.common.di.ViewModelKey
import com.example.presentation.common.di.ViewModelProviderFactory
import com.example.presentation.ui.main.fragment.first.FirstFragment
import com.example.presentation.ui.main.fragment.first.FirstFragmentViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap


@Module
abstract class PresentationModule{

    @Binds
    abstract fun bindViewModelFactory(viewModelProviderFactory: ViewModelProviderFactory): ViewModelProvider.Factory

    @ContributesAndroidInjector
    abstract fun firstFragmentInjector(): FirstFragment

    @Binds
    @IntoMap
    @ViewModelKey(FirstFragmentViewModel::class)
    abstract fun viewModel(firstFragmentViewModel: FirstFragmentViewModel): ViewModel



}
package com.example.presentation.ui.main.fragment.first

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.entity.ResultEntity
import javax.inject.Inject


class FirstFragmentViewModel @Inject constructor() : ViewModel() {
   var resultEntity= ResultEntity(0, 0, 0)
    val result: MutableLiveData<ResultEntity> = MutableLiveData()
    fun placement(numberOfWords: Int) {
        when {
            numberOfWords < 100 -> {
                resultEntity.level = 0
                resultEntity.remainingWords= 100 - numberOfWords
                resultEntity.progress=(numberOfWords*100)/100

            }
            numberOfWords < 500 -> {
                resultEntity.level = 1
                resultEntity.remainingWords = 500 - numberOfWords
                resultEntity.progress=((numberOfWords-100)*100)/400

            }
            numberOfWords < 2000 -> {
                resultEntity. level = 2
                resultEntity. remainingWords = 2000 - numberOfWords
                resultEntity.progress=((numberOfWords-500)*100)/1500

            }
            numberOfWords < 6000 -> {
                resultEntity.  level = 3
                resultEntity.remainingWords = 6000 - numberOfWords
                resultEntity.progress=((numberOfWords-2000)*100)/4000

            }
            numberOfWords < 12000 -> {
                resultEntity.   level = 4
                resultEntity.  remainingWords = 12000 - numberOfWords
                resultEntity.progress=((numberOfWords-6000)*100)/6000

            }
            numberOfWords < 20000 -> {
                resultEntity.    level = 5
                resultEntity.  remainingWords = 20000 - numberOfWords
                resultEntity.progress=((numberOfWords-12000)*100)/8000


            }
            else -> {
                resultEntity. level = 5
                resultEntity. remainingWords = 0
                resultEntity.progress=(numberOfWords*100)/100

            }
        }
        this.result.value =resultEntity

    }

}
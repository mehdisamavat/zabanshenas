package com.example.presentation.common.view

import android.widget.Toast
import androidx.annotation.StringRes
import dagger.android.support.DaggerFragment



abstract class BaseFragment : DaggerFragment() {



    fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }




}
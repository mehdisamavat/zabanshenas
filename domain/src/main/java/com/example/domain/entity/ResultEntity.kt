package com.example.domain.entity

data class ResultEntity(var level:Int,var remainingWords:Int,var progress:Int)
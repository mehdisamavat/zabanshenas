package com.example.presentation.ui.main.fragment.first

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.domain.entity.ResultEntity
import com.example.presentation.R
import com.example.presentation.common.di.ViewModelProviderFactory
import com.example.presentation.common.extention.observe
import com.example.presentation.common.extention.viewModelProvider
import com.example.presentation.common.view.BaseFragment
import kotlinx.android.synthetic.main.fragment_first.*
import javax.inject.Inject


class FirstFragment : BaseFragment() {


    @Inject
    lateinit var factory: ViewModelProviderFactory
    lateinit var viewModel: FirstFragmentViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModelProvider(factory)
        okButton.setOnClickListener {
            if (numbeerOfWordEditText.text.toString().isNotEmpty()) {
                viewModel.placement(numbeerOfWordEditText.text.toString().toInt())
            } else showMessage("فیلد را پر کنید")
        }
        observe(viewModel.result, ::updateUi)
    }

    private fun updateUi(resultEntity: ResultEntity) {
        levelInfoTextView.text = if (resultEntity.remainingWords != 0) {
            "سطح ${resultEntity.level} \n ${resultEntity.remainingWords} لغت تا سطح بعد "
        } else {
            "سطح ${resultEntity.level}"
        }
        progressBarHorizontal.progress = resultEntity.progress
        progressBarTextView.text = "${progressBarHorizontal.progress}% "
    }
}

package com.example.zabanshenas

import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Singleton
@Component(modules = [AppModule::class])
interface AppComponent :AndroidInjector<App>{
    @Component.Factory
    interface Factory:AndroidInjector.Factory<App>
}